# -*- coding: utf-8 -*-
import asyncio
import logging

import settings
from chip.run import init_controller, init_webserver, link_urls


async def send_response(writer, response):
    if response:
        print('Write response')
        writer.write(response.as_text())
    writer.close()


def main():
    logging.basicConfig(level=getattr(logging, settings.LOGLEVEL))
    logging.info('Init webserver...')
    loop = asyncio.get_event_loop()
    srv = init_webserver(loop, port=8080)
    srv.send_response = send_response
    logging.info('Init controller...')
    cntrl = init_controller()
    link_urls(srv, cntrl)
    asyncio.gather(srv.start(), cntrl.run())
    logging.info('Run forever.')
    loop.run_forever()


if __name__ == '__main__':
    main()
