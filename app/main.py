# -*- coding: utf-8 -*-

import kivy
from kivy.app import App

from sources.layout import MoonshineLayout

kivy.require('2.0.0')


class MoonshineApp(App):

    def build(self):
        return MoonshineLayout()


if __name__ == "__main__":
    app = MoonshineApp()
    app.run()
