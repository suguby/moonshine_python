# -*- coding: utf-8 -*-
import time
from threading import Thread

import requests
from kivy.clock import Clock
from kivy.uix.boxlayout import BoxLayout


esp_ip = '192.168.1.200'
esp_data = {}
esp_put = {}


def get_esp_status(dt):
    global esp_data
    try:
        res = requests.get(f'http://{esp_ip}/status', timeout=.9)
        esp_data = res.json()
    except Exception as exc:
        print(exc)


class MoonshineLayout(BoxLayout):
    ESP_IP = esp_ip
    power_value = 100
    temperature_cube = 0
    temperature_column = 0

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def on_add_power(self):
        self.power_value += 1
        if self.power_value > 100:
            self.power_value = 100
        self.ids.power_value_widget.text = str(self.power_value)

    def on_reduce_power(self):
        self.power_value -= 1
        if self.power_value < 0:
            self.power_value = 0
        self.ids.power_value_widget.text = str(self.power_value)

    def on_edit_power(self):
        text = str(self.ids.power_value_widget.text)
        if text == '':
            return
        if not text.isdigit():
            self.ids.power_value_widget.text = str(self.power_value)
            return
        self.power_value = int(text)
        if self.power_value < 0:
            self.power_value = 0
        if self.power_value > 100:
            self.power_value = 100
        self.ids.power_value_widget.text = str(self.power_value)

    def on_connect(self):
        ip = self.ids.ip_address_widget.text


# event = Clock.schedule_interval(get_esp_status, 1)
