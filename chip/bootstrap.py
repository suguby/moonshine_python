# -*- coding: utf-8 -*-

try:
    import uasyncio as asyncio
    import network as network
    import machine
    import onewire
    import ds18x20
    import uio

    def get_traceback(exc):
        import sys
        buf = uio.StringIO()
        sys.print_exception(exc, buf)
        buf.seek(0)
        return buf.read().split('\n')


except ImportError:
    import asyncio
    from unittest import mock
    import io as uio

    network = mock.MagicMock()
    machine = mock.MagicMock()
    onewire = mock.MagicMock()
    ds18x20 = mock.MagicMock()

    def get_traceback(exc):
        import traceback
        return traceback.format_exc().split('\n')
