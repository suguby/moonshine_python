# -*- coding: utf-8 -*-
import logging

import settings

from bootstrap import asyncio
from controller import Controller
from utils import init_settings, up_network
from webserver import AWebServer, Response200


async def do_something(request):
    return Response200(['Самогон!'])


def init_webserver(loop, port=80):
    srv = AWebServer(loop=loop, port=port)
    srv.add_route('/samo', do_something)
    return srv


def init_controller():
    return Controller()


def link_urls(server, controller):
    server.add_route('/status', controller.get_status)
    server.add_route('/rule', controller.put_commands, method='POST')


def main():
    logging.info('Init settings...')
    init_settings()
    logging.basicConfig(level=getattr(logging, settings.LOGLEVEL))
    logging.info('Starting network...')
    mess = up_network()
    logging.info(mess)
    logging.info('Init webserver...')
    loop = asyncio.get_event_loop()
    srv = init_webserver(loop)
    logging.info('Init controller...')
    cntrl = init_controller()
    link_urls(srv, cntrl)
    logging.info('Run forever')
    tsk = asyncio.gather(srv.start(), cntrl.run())
    loop.create_task(tsk)
    loop.run_forever()


if __name__ == 'run':
    main()
