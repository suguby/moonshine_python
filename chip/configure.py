# -*- coding: utf-8 -*-

import settings
from utils import load_data, save_data


def main():
    conf = load_data(file_name=settings.SETTINGS_FILE_NAME)
    for var_name in dir(settings):
        if var_name == 'SETTINGS_FILE_NAME' or var_name.upper() != var_name:
            continue
        old_val = conf[var_name] if var_name in conf else getattr(settings, var_name)
        val = input(f'What {var_name}? (enter to leave "{old_val}") ')
        if not val:
            val = old_val
        conf[var_name] = val
    save_data(conf, file_name=settings.SETTINGS_FILE_NAME)


if __name__ == 'configure':
    main()
