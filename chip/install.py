# -*- coding: utf-8 -*-

import upip
from utils import init_settings, up_network

PACKAGES = [
    'micropython-logging',
    'micropython-traceback',
    'micropython-uio',
]

init_settings()
up_network()

for package in PACKAGES:
    print(f'============================== Try to install {package} ===')
    upip.install(package)
