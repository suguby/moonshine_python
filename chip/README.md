# Контроллер самогона

### Прошивка micropython 
see http://docs.micropython.org/en/latest/esp8266/tutorial/intro.html

https://micropython.org/download/?port=esp8266
https://micropython.org/download/esp8266/

```bash
export MICROPYTHON_VERSION=esp8266-20220117-v1.18.bin
wget https://micropython.org/resources/firmware/${MICROPYTHON_VERSION}
pip install esptool==3.0 
esptool.py --port /dev/ttyUSB0 erase_flash
esptool.py --port /dev/ttyUSB0 --baud 115200 write_flash --flash_size=detect --verify 0 ${MICROPYTHON_VERSION} 

```

### Установка пакетов на борду

```python
import network # Импорт библиотеки
sta_if = network.WLAN(network.STA_IF) # Режим приёмника
sta_if.active(True)
sta_if.connect('<your ESSID>', '<your password>')
import upip
upip.install("pakage_name")

```
Инсталлит оно отсюда https://github.com/pfalcon/pycopy-lib

Сделал скрипт типа "pip install -r requirements.txt" 
Надо сделать `import install` на борде, список необходимых пакетов вшит в install.py 

Использование upip - https://pycopy.readthedocs.io/en/latest/reference/packages.html?highlight=upip#upip-package-manager

### Загрузка контроллера 

### Проверить какие файлы на чипе

