# -*- coding: utf-8 -*-

import json
import re

from bootstrap import asyncio, get_traceback


class Request:

    def __init__(self, raw_request):
        print(raw_request)
        headers, body = raw_request.decode().split('\r\n\r\n')
        print(headers, body)
        self.headers = headers.split('\r\n')
        first_line = self.headers.pop(0)
        self.method = re.search('^([A-Z]+)', first_line).group(1)
        self.path = re.search('^[A-Z]+\\s+(/[-a-zA-Z0-9_.]*)', first_line).group(1)
        self.body = body

    def __str__(self):
        res = [f'{self.method} {self.path}']
        res.extend(self.headers)
        res.append('')
        res.append(self.body)
        return '\r\n'.join(res)


class Response:
    CODE = None
    DESC = None

    def __init__(self, payload):
        if self.CODE is None:
            raise NotImplementedError('Response code undefined')
        if isinstance(payload, str):
            self.payload = payload
        else:
            self.payload = json.dumps(payload)

    def as_text(self):
        res = [
            'HTTP/1.0 {} {}'.format(self.CODE, self.DESC),
            'Content-Type: application/json;charset=UTF-8',
            'Connection: close',
            '',
            self.payload,
        ]
        res = '\n'.join(res)
        return res.encode('utf8')


class Response200(Response):
    CODE = 200
    DESC = 'OK'


class Response400(Response):
    CODE = 400
    DESC = 'Bad request'


class Response404(Response):
    CODE = 404
    DESC = 'Not found'


class Response500(Response):
    CODE = 500
    DESC = 'Internal Server Error'


class AWebServer:
    loop = None

    def __init__(self, host='0.0.0.0', port=80, loop=None):
        if loop is None and self.loop is None:
            raise ValueError('You must specify async loop!')
        if loop is not None:
            self.loop = loop
        self._host = host
        self._port = port
        self._routes = []
        self._on_request_handler = None
        self._on_not_found_handler = None
        self._on_error_handler = None

    def add_route(self, path, handler, method='GET'):
        self._routes.append(
            {'path': path, 'handler': handler, 'method': method})

    def on_request(self, handler):
        self._on_request_handler = handler

    def on_not_found(self, handler):
        self._on_not_found_handler = handler

    def on_error(self, handler):
        self._on_error_handler = handler

    async def start(self):
        await asyncio.start_server(self._handle_client, self._host, self._port)

    async def _handle_client(self, reader, writer):
        raw_request = await reader.read(1024)
        response = None
        try:
            if len(raw_request) == 0:
                return Response400('Empty request')
            request = Request(raw_request)
            print(request)
            response = await self._get_response(request)
            print(response)
            if not isinstance(response, Response):
                response = Response500('Illegal response type {}'.format(type(response)))
        except Exception as exc:
            response = await self._internal_error(raw_request, exc)
        finally:
            await self.send_response(writer, response)

    async def send_response(self, writer, response):
        if response:
            as_text = response.as_text()
            print('Write response', as_text)
            await writer.awrite(as_text)
        await writer.aclose()

    async def _get_response(self, request: Request):
        if self._on_request_handler:
            return await self._on_request_handler(request)
        route = self.find_route(request)
        if route:
            return await route['handler'](request)
        if self._on_not_found_handler:
            return await self._on_not_found_handler(request)
        return await self._route_not_found(request)

    def find_route(self, request: Request):
        for route in self._routes:
            if request.method != route['method']:
                continue
            if request.path == route['path']:
                return route
            else:
                match = re.search('^' + route['path'] + '$', request.path)
                if match:
                    return route

    async def _route_not_found(self, request: Request):
        return Response404(f'Not found path {request.path} (on {request.method})')

    async def _internal_error(self, raw_request, exc):
        if self._on_error_handler:
            return await self._on_error_handler(raw_request, exc)
        res = ['Error: {}'.format(exc)]
        res.extend(get_traceback(exc))
        res.extend(['', 'Raw request:'])
        res.extend(raw_request.decode('utf8', 'ignore').split('\n'))
        return Response500(res)
