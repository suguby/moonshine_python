# -*- coding: utf-8 -*-
import json
import sys
import time

import settings
from bootstrap import network


def save_data(data, file_name):
    file_name = file_name
    with open(file_name, 'w') as ff:
        ff.write(json.dumps(data))


def load_data(file_name) -> dict:
    file_name = file_name
    try:
        with open(file_name, 'r') as ff:
            raw_data = ff.read()
    except OSError:
        return {}
    if not raw_data:
        return {}
    return json.loads(raw_data)


def up_network():
    # TODO отваливаться при нескольких неудачных попытках
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        wlan.connect(settings.SSID, settings.WIFI_PASSWD)
        while not wlan.isconnected():
            time.sleep(1)
    return f'Connected to {settings.SSID}. Device IP is {wlan.ifconfig()[0]}'


def init_settings():
    conf = load_data(file_name=settings.SETTINGS_FILE_NAME)
    if not conf:
        print('Cant load settings!!! Run configure first!!!')
        sys.exit(1)
    for var_name in dir(settings):
        if var_name == 'SETTINGS_FILE_NAME' or var_name.upper() != var_name:
            continue
        if var_name not in conf:
            print('No %s in settings! Run configure first!!!', var_name)
            sys.exit(1)
        setattr(settings, var_name, conf[var_name])
