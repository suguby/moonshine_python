# -*- coding: utf-8 -*-

import json
import logging
import time

import settings
from bootstrap import asyncio
from webserver import Response200


class SamoData:

    def __init__(self):
        self.data = {}

    def set_temperature(self, name, value):
        if 'temperature' not in self.data:
            self.data['temperature'] = {}
        self.data['temperature'][name] = value

    def as_json(self):
        return json.dumps(self.data)


class Controller:

    def __init__(self):
        self.status = SamoData()

    async def get_status(self, request):
        return Response200(self.status.as_json())

    async def put_commands(self, request):
        logging.info('Commands: %s', request)

    async def run(self):
        while True:
            begin = time.time()
            await self._step()
            sleep_time = settings.CONTROLLER_TICK - (time.time() - begin)
            if sleep_time > 0:
                await asyncio.sleep(sleep_time)
            else:
                print(f'Controller run out time slot - {sleep_time}')

    async def _step(self):
        await self.check_temperature()

    async def check_temperature(self):
        self.status.set_temperature('cube', 86.6)
        self.status.set_temperature('column', 27.7)
