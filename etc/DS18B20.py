# -*- coding: utf-8 -*-

import time

print('Before import')
import machine
import onewire
import ds18x20
print('After import')


def probe_temperature():
    ds_pin = machine.Pin(2)
    print('ds_pin', ds_pin)
    ds_sensor = ds18x20.DS18X20(onewire.OneWire(ds_pin))
    print('ds_sensor', ds_sensor)

    roms = ds_sensor.scan()
    print('Found DS devices: ', roms)
    while True:
        ds_sensor.convert_temp()
        time.sleep_ms(750)
        for rom in roms:
            print(rom)
            print(ds_sensor.read_temp(rom))
            time.sleep(.1)
