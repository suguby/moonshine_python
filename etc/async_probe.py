# -*- coding: utf-8 -*-

import network
import uasyncio as asyncio
from machine import Pin
import uasyncio

led5 = Pin(2, Pin.OUT)  # D1
led4 = Pin(4, Pin.OUT)  # D2


async def lend_1():
    count1 = 0
    i1 = 0
    while True:
        count1 += 1
        print('count1=')
        print(count1)
        if i1 == 0:
            led4.off()
            i1 = 1
        else:
            led4.on()
            i1 = 0
        await asyncio.sleep(.2)


async def lend_2():
    count1 = 0
    i1 = 0
    while True:
        count1 += 1
        print('count2=')
        print(count1)
        if i1 == 0:
            led4.off()
            i1 = 1
        else:
            led4.on()
            i1 = 0
        await asyncio.sleep(1)


def a_probe():
    loop = asyncio.get_event_loop()
    loop.create_task(lend_1())
    loop.create_task(lend_2())

    loop.run_forever()
