# -*- coding: utf-8 -*-


import machine
import time

pin = machine.Pin(2, machine.Pin.OUT)


def dot():
    pin.off()
    time.sleep(.1)
    pin.on()
    time.sleep(.3)
    print('dot')


def dash():
    pin.off()
    time.sleep(.3)
    pin.on()
    time.sleep(.3)
    print('dash')


def sos():
    while True:
        dot()
        dot()
        dot()
        dash()
        dash()
        dash()
